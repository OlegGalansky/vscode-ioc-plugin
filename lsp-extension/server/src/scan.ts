import { map } from "lodash";
import { promises as fs } from "fs";
import { Dependency } from "./type";
import { DependencyList } from "./type";


export async function findDepsByRegExp(
  files: string[],
  regExp: RegExp
): Promise<DependencyList> {
  const dependencyList: DependencyList = { dependencies: [] };
  const pendingFiles = files.map(async (file) => {
    const data = await grepFile(file, regExp);
    if (data) {
      dependencyList[file] = data;
      dependencyList.dependencies.push(...map(data, "dependencyName"));
    }
  });
  await Promise.all(pendingFiles);

  return dependencyList;
}


async function readFile(filePath: string, fsOpts: Record<string, unknown>): Promise<string[]> {
  const data = await fs.readFile(filePath, fsOpts);
  return data.toString().split(/\n/);
}

async function grepFile(
  filePath: string,
  resolveRegExp = /resolve\(['"].+['"]\)/,
  depRegExp = /(?<=["'])[^"']+/
): Promise<Dependency[]> {
  const res: Dependency[] = [];
  const fsOpts = { flag: "r", encoding: "utf8" };
  const convertedData = await readFile(filePath, fsOpts);

  for (let i = 0; i < convertedData.length; i++) {
    const line = convertedData[i];
    if (!resolveRegExp.test(line)) continue;

    const matchedStr = line.match(resolveRegExp);

    if (!matchedStr) {
      throw new Error();
    }

    const depName = matchedStr[0].match(depRegExp);

    if (!depName) {
      throw new Error();
    }

    res.push({
      strNumber: i + 1,
      dependencyName: depName[0],
      resolveStr: matchedStr[0],
      index: matchedStr.index,
      input: matchedStr.input,
    });
  }

  return res;
}
