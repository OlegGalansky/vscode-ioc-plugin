import { existsSync, promises as fs } from "fs";
import { map } from "lodash";
import { SNIPPET_PATH } from "./consts";
import { grepFile } from "./grepFile";
import { DependencyList } from "./type";

export async function findDepsByRegExp(
  files: string[],
  regExp: RegExp
): Promise<DependencyList> {
  const dependencyList: DependencyList = { dependencies: [] };
  const pendingFiles = files.map(async (file) => {
    const data = await grepFile(file, regExp);
    if (data) {
      dependencyList[file] = data;
      dependencyList.dependencies.push(...map(data, "dependencyName"));
    }
  });
  await Promise.all(pendingFiles);

  return dependencyList;
}

export function createErrors(
  resolveDeps: DependencyList,
  registerDeps: DependencyList
) {
  let err = "";
  for (const key in resolveDeps) {
    for (const dep of resolveDeps[key]) {
      if (dep.dependencyName && !registerDeps.dependencies.includes(dep.dependencyName)) {
        //TODO Strange cast with magic Const
        err +=
          `\n The dep in file ${key} at ${dep.strNumber}:${dep!.index as number + 8} ` +
          `with name {${dep.dependencyName}} not defined`;
      }
    }
  }

  return err;
}

export async function createSnippet(
  path: string,
  registerDeps: DependencyList
) {

  const snippet = {
    res: {
      prefix: ["resolve"],
      body: [
        "resolve(${1|" +
          '"' +
          registerDeps.dependencies.join('","') +
          '"' +
          "|})",
      ],
      description: "resolve",
    },
  };
  const stringifySnippet = JSON.stringify(snippet, null, 4);

  if (!existsSync(path + "/.vscode")) {
    await fs.mkdir(path + "/.vscode");
  }

  if (!existsSync(path + SNIPPET_PATH)) {
    await fs.writeFile(path + SNIPPET_PATH, stringifySnippet);
  } else {
    const content = await fs.readFile(path + SNIPPET_PATH);
    const parsedContent = JSON.parse(content.toString());
    parsedContent.res = snippet.res;
    const newData = JSON.stringify(parsedContent, null, 4);
    await fs.writeFile(path + SNIPPET_PATH, newData);
  }
}
