export const SRC = "testing/";
export const RES_REGEXP = /resolve\(['"].+['"]\)/;
export const REG_REGEXP = /register\(['"].+['"]\)/;
export const FILES_REGEXP = "/**/*.ts";
export const SNIPPET_PATH = "/.vscode/js.code-snippets";
