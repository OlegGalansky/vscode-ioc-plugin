export type Dependency = {
  strNumber: number;
  dependencyName: string;
  resolveStr: string;
  //TODO: type assertion
  index: number | undefined;
  input: string | undefined;
};

export type DependencyList = { [k: string]: Dependency[] } & {
  dependencies: string[];
};
