import * as vscode from "vscode";
import { promises as fs } from "fs";
import * as fg from "fast-glob";
import { createErrors, createSnippet, findDepsByRegExp } from "./test";
import { FILES_REGEXP, REG_REGEXP, RES_REGEXP } from "./consts";

export function activate(context: vscode.ExtensionContext) {
  let message = "";
  console.log('Congratulations, your extension "iocplugin" is now active!');
  let disposable = vscode.commands.registerCommand(
    "iocplugin.helloWorld",
    async () => {
      console.log(vscode.workspace.workspaceFolders);

      if (vscode.workspace.workspaceFolders !== undefined) {
        const path = vscode.workspace.workspaceFolders[0].uri.path;
        const files = await fg([path + FILES_REGEXP], { dot: false });
        const resolveDeps = await findDepsByRegExp(files, RES_REGEXP);
        const resisterDeps = await findDepsByRegExp(files, REG_REGEXP);
        console.log(resolveDeps);
        console.log(resolveDeps);

        const err = createErrors(resolveDeps, resisterDeps);
        console.log(err);

        const output = vscode.window.createOutputChannel("Dawn");
        output.show();
        output.append(err);
        vscode.window.showErrorMessage(err);

        await createSnippet(path, resisterDeps);
      } else {
        message =
          "YOUR-EXTENSION: Working folder not found, open a folder an try again";
        vscode.window.showErrorMessage(message);
        vscode.window.showInformationMessage("asdsad");
      }
    }
  );

  context.subscriptions.push(disposable);
}

export function deactivate() {}
